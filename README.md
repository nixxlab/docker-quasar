# Quasar CLI

Docker image based on `node:11-alpine`.


## Development environment

Console: `docker run --rm -ti -p 8080:8080 -v $PWD:/app:rw nixxlab/quasar sh`

Create new project: `docker run --rm -ti -v $PWD:/app:rw nixxlab/quasar quasar create myproject`

Install dependencies: `docker run --rm -ti -v $PWD:/app:rw nixxlab/quasar yarn`

Serve: `docker run --rm -ti -v $PWD:/app:rw -p 8080:8080 nixxlab/quasar quasar dev`

For sync files you can edit `quasar.config.js` in the project folder and add next options to `devServer` section:
```
  ...
    devServer: {
      watchOptions: {
        ignored: /node_modules/,
        poll: 1000
      }
    }
  ...
}
```

## Production environment

Build: `docker run --rm -ti -v $PWD:/app:rw nixxlab/quasar quasar build`